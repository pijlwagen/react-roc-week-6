# React ROC week 6
Components, libraries & meer.

In de voorgaande weken heb je geleerd hoe je componenten herkent, maakt en implementeert. Nu is het tijd om deze taak grotendeels bij iemand anders te leggen, namelijk bij een library. Waarom zou je zelf componenten maken, als je ook gewoon die van andere kan gebruiken!

Ik introduceer u allen aan: [React Bootstrap](https://react-bootstrap.netlify.app/). React Bootstrap is een verzameling van Bootstrap componenten, maar dan voor React. Eigenlijk zoals onze button en card die we in week 3 hebben gemaakt, maar dan voor alle Bootstrap componenten. Naast React Boostrap zijn er nog veel meer libraries voor React, zoals Material UI of Ant Design.

Het voordeel aan het gebruiken van een component library is dat je zelf weinig componenten hoeven te maken, want je kan gewoon die van React Bootstrap gebruiken. Deze componenten kunnen we ook tot op een bepaalt niveau aanpassen naar je eigen smaak. 
 
Om gebruik te kunnen maken van React Bootstrap, zul je eerts met NPM deze library naar je toe moeten halen. Maak een nieuw React project en installeer daarin `react-bootstrap sass bootstrap`. Dit kun je doen met het commando `npm install sass react-bootstrap bootstrap`.

Als je React Bootstrap hebt geinstalleerd kun je eigenlijk direct al aan de slag met de componenten. Het is belangrijk dat je alleen componenten importeert die je ook daadwerkelijk gaat gebruiken, waarom is dat belangrijk? Om aan het einde van de rit als je naar production gaat deployen een zo klein mogelijk bestand op te leveren.


## Huiswerk
U raad het al, de app die vorige week is gemaakt mag je nu herschrijven zodat die gebruik maakt van de componenten van React Bootstrap.
- Gebruik zo veel mogelijk van de componenten in React Bootstrap.
- Dit geldt ook voor de [Container, Row en Col](https://react-bootstrap.netlify.app/docs/layout/grid).

Loop je vast, check eerst de documentatie van React Bootstrap. Kom je er niet uit? Kom dan bij mij langs (of op Discord)!
